import java.util.Scanner ;

public class Ex4 {

    public static void main (String[]args) {

        int n1, n2;
        int operacao = -1;
        boolean reiniciar = true;

        while (reiniciar == true) {

            Scanner captura = new Scanner(System.in);

            System.out.println("==== Calculadora ==== ");
            System.out.println("Digite um número: ");
            n1 = captura.nextInt();
            System.out.println("Digite outro número: ");
            n2 = captura.nextInt();

            while (operacao < 1 || operacao > 4) {

                digitaMenu();
                Scanner dados = new Scanner(System.in);
                operacao = dados.nextInt();

            }

            int resultado = realizaOperacao(operacao, n1, n2);
            System.out.println("O resultado da operacao é: " + resultado);

        }
    }

    public static int realizaOperacao (int menuDeOperacoes, int n1, int n2) {

        int retorno;

        switch (menuDeOperacoes) {
            case 1:
                retorno = soma(n1, n2);
                break;
            case 2:
                retorno = subtracao(n1, n2);
                break;
            case 3:
                retorno = divisao(n1, n2);
                break;
            case 4:
                retorno = multiplicacao(n1, n2);
                break;
            default:
                retorno = -1;
        }

        return retorno;
    }

    public static int soma(int n1, int n2) {
        System.out.println("soma");
        return n1 + n2;
    }


    public static int subtracao(int n1, int n2) {
        System.out.println("subtracao");
        return n1 - n2;
    }

    public static int divisao(int n1, int n2){
        System.out.println("divisao");
        return n1 / n2;
    }


    public static int multiplicacao (int n1, int n2){
        System.out.println("multiplicacao");
        return n1 * n2;
    }


    public static void digitaMenu(){

        System.out.println("Menu de operações:");

        Scanner dados = new Scanner(System.in);
        System.out.println("Digite o número referente a operação desejada:");
        System.out.println(" 1 - Soma");
        System.out.println(" 2 - Subtração");
        System.out.println(" 3 - Divisão");
        System.out.println(" 4 - Multiplicação");


    }
}